import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../reminders.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private reminderService:RemindersService) { }
  login()
  {
    this.reminderService.setLoginStatus(true);
  }
  ngOnInit(): void {
  }

}
