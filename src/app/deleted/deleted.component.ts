import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../reminders.service';

@Component({
  selector: 'app-deleted',
  templateUrl: './deleted.component.html',
  styleUrls: ['./deleted.component.css']
})
export class DeletedComponent implements OnInit {

  title = '';
  index;
  desc = '';
  deletedList =[]
  newTitle:string='';
  newDesc:string='';
  constructor(private reminderService:RemindersService) { }
  

  ngOnInit(): void {
    this.deletedList=this.reminderService.getDeletedList();
  }
restoreTask()
  {
    this.deletedList=this.reminderService.getDeletedList();
    let task=this.deletedList[this.index];
    this.deletedList.splice(this.index,1);
    this.reminderService.setDeletedList(this.deletedList);
    let dashboardList=this.reminderService.getDashboardList();
    dashboardList.push(task);
    this.reminderService.setDashboardList(dashboardList);
  }
}
