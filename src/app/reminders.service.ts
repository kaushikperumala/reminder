import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RemindersService {
  reminderList = [
    { title: "Payment", desc: "Pay Credit Card Bills" },
    { title: "Payment", desc: "Pay Credit Card Bills" },
    { title: "Payment", desc: "Pay Credit Card Bills" },
    { title: "Payment", desc: "Pay Credit Card Bills" },
    { title: "Payment", desc: "Pay Credit Card Bills" },
    { title: "Payment", desc: "Pay Credit Card Bills" }
  ]

  dashboardList = [
    { title: "Payment", desc: "Pay Credit Card Bills", id: "#task1" },
    { title: "Payment", desc: "Pay Credit Card Bills", id: "#task2" },
    { title: "Payment", desc: "Pay Credit Card Bills", id: "#task3" },
  ]
  deletedList = [
    { title: "Payment", desc: "Pay Credit Card Bills" }
  ];
  login = false;
  constructor() { }
  getReminderList() {
    return this.reminderList;
  }
  getDeletedList() { return this.deletedList; }
  setReminderList(reminderList) { this.reminderList = reminderList; }
  setDeletedList(deletedList) { this.deletedList = deletedList; }
  getLoginStatus() { return this.login; }
  setLoginStatus(login) { this.login = login; }
  getDashboardList() { return this.dashboardList; }
  setDashboardList(dashboardList) { this.dashboardList = dashboardList; }
}
