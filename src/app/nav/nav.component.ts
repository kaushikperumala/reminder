import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../reminders.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  title = 'reminder';
  show = false;
  username:string="Thopulake Thopu";

  constructor(private reminderService:RemindersService) { }

  ngOnInit(): void {
  }
  
  toggleMenu()
  {
    if(this.reminderService.getLoginStatus())
    this.show=!this.show;
  }
  logout()
  {
    this.show=false;  
    this.reminderService.setLoginStatus(false);
  }

}
