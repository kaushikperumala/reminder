import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../reminders.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = '';
  desc = '';
  index;
  dashboardList=[];
  newTitle:string='';
  newDesc:string='';
  currentTask = '';
  
  constructor(private reminderService:RemindersService) { }

  ngOnInit(): void { 
    this.dashboardList=this.reminderService.dashboardList;
    console.log(this.dashboardList)
  }

  editTask()
  {
    let task={
      title:this.newTitle,
      desc:this.newDesc
    };
    this.newTitle='';
    this.newDesc='';
    this.dashboardList.splice(this.index,1,task);
    this.reminderService.setDashboardList(this.dashboardList);
  }
  preEdit()
  {  
    let task=this.reminderService.getDashboardList()[this.index];
    this.newDesc=task.desc;
    this.newTitle=task.title;
    console.log(this.currentTask)
  }

  addTask()
  {
    let task = {
      title: this.newTitle,
      desc: this.newDesc
    };
    this.newTitle='';
    this.newDesc='';
    this.dashboardList.push(task);
    this.reminderService.setDashboardList(this.dashboardList);
  }
  deleteTask()
  {
    this.dashboardList=this.reminderService.getDashboardList();
    let task=this.dashboardList[this.index];
    this.dashboardList.splice(this.index,1);
    let deleted=this.reminderService.getDeletedList();
    deleted.push(task);
    this.reminderService.setDeletedList(deleted);
    this.reminderService.setDashboardList(this.dashboardList);
  }

  moveToCompleted()
  {
    this.dashboardList=this.reminderService.getDashboardList();
    let task=this.dashboardList[this.index];
    this.dashboardList.splice(this.index,1);
    this.reminderService.setDashboardList(this.dashboardList);
    let reminderList= this.reminderService.getReminderList();
    reminderList.push(task);
    this.reminderService.setReminderList(reminderList);
  }

}
