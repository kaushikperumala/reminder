import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../reminders.service';

@Component({
  selector: 'app-completed',
  templateUrl: './completed.component.html',
  styleUrls: ['./completed.component.css']
})
export class CompletedComponent implements OnInit {

  title = '';
  desc = '';
  index;
  reminderList=[];
  newTitle:string='';
  newDesc:string='';
  
  constructor(private reminderService:RemindersService) { }

  ngOnInit(): void { 
    this.reminderList=this.reminderService.getReminderList();
  }

  editTask()
  {
    let task={
      title:this.newTitle,
      desc:this.newDesc
    };
    this.newTitle='';
    this.newDesc='';
    this.reminderList.splice(this.index,1,task);
    this.reminderService.setReminderList(this.reminderList);
  }
  preEdit()
  {  
    let task=this.reminderService.getReminderList()[this.index];
    this.newDesc=task.desc;
    this.newTitle=task.title;
  }

  addTask()
  {
    let task = {
      title: this.newTitle,
      desc: this.newDesc
    };
    this.newTitle='';
    this.newDesc='';
    this.reminderList.push(task);
    this.reminderService.setReminderList(this.reminderList);
  }
  deleteTask()
  {
    this.reminderList=this.reminderService.getReminderList();
    let task=this.reminderList[this.index];
    this.reminderList.splice(this.index,1);
    let deleted=this.reminderService.getDeletedList();
    deleted.push(task);
    this.reminderService.setDeletedList(deleted);
    this.reminderService.setReminderList(this.reminderList);
  }

}
