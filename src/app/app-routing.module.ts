import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CompletedComponent} from './completed/completed.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DeletedComponent } from './deleted/deleted.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'dashboard',component:DashboardComponent},
  {path: 'completed', component:CompletedComponent},
  {path:'deleted',component:DeletedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
